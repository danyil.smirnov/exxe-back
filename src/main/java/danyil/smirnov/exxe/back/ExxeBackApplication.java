package danyil.smirnov.exxe.back;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExxeBackApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExxeBackApplication.class, args);
    }

}
